package ml.mackenziemolloy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

import static org.bukkit.Bukkit.getServer;

public class Commands implements CommandExecutor {

    private final Main main;

    public Commands(final Main main) {
        this.main = main;
        main.getCommand("lobbyfishing").setExecutor((CommandExecutor) this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, final String[] args) {

        if(sender instanceof Player) {
            Player p = (Player) sender;

            if (args.length <= 0) {
                List<String> invalidArgsRaw = main.getSettings().getProperty(Config.MESSAGES_HELP);
                String msg = "\n";

                for(int i = 0; i < invalidArgsRaw.size(); i++) {
                    msg = msg + invalidArgsRaw.get(i) + "\n";
                }

                String invalidArgs = ChatColor.translateAlternateColorCodes('&', msg);
                p.sendMessage(invalidArgs);
                return true;
            }

            else if(args[0].toLowerCase().equals("reload") || args[0].toLowerCase().equals("rl")) {
                if(p.hasPermission("lobbyfishing.reload")) {
                    main.getSettings().reload();
                    main.loadData();

                    String reloaded = main.getSettings().getProperty(Config.MESSAGES_PLUGIN_RELOADED);
                    String msg = ChatColor.translateAlternateColorCodes('&', reloaded);

                    p.sendMessage(msg);
                }
                else {
                    String noPermissionRaw = main.getSettings().getProperty(Config.MESSAGES_NO_PERMISSION);
                    String noPermission = ChatColor.translateAlternateColorCodes('&', noPermissionRaw);

                    p.sendMessage(noPermission);
                }
            }

            else if(args[0].toLowerCase().equals("sell")) {

                ItemStack heldItem = p.getInventory().getItemInMainHand();

                if(String.valueOf(heldItem.getType()) != "LEGACY_RAW_FISH") {
                    String notSellableRaw = main.getSettings().getProperty(Config.MESSAGES_NOT_SELLABLE);
                    String notSellable = ChatColor.translateAlternateColorCodes('&', notSellableRaw);

                    p.sendMessage(notSellable);
                    return true;

                }

                List<String> raritiesColor = main.getSettings().getProperty(Config.MESSAGES_FISH_RARITIES);

                for(int i = 0; i < raritiesColor.size(); i++) {

                    String rarity = ChatColor.translateAlternateColorCodes('&', raritiesColor.get(i));

                    if(p.getInventory().getItemInMainHand().getItemMeta().getDisplayName().startsWith(rarity)) {

                        int quantity = p.getInventory().getItemInMainHand().getAmount();

                        int amount = (i+1) * quantity;

                        String gainedPointsRaw = main.getSettings().getProperty(Config.MESSAGES_POINTS_GAINED).replace("{gained}", String.valueOf(amount)).replace("{quantity}", String.valueOf(quantity));
                        String gainedPoints = ChatColor.translateAlternateColorCodes('&', gainedPointsRaw);

                        p.sendMessage(gainedPoints);
                        p.getInventory().setItemInMainHand(new ItemStack(Material.AIR));

                        if(main.getplayerData().get(String.valueOf(p.getUniqueId())) == null) {
                            main.getplayerData().set(String.valueOf(p.getUniqueId()), Integer.valueOf(amount));

                            main.saveData();
                            break;
                        }

                        int pointsAmount = (int) main.getplayerData().get(String.valueOf(p.getUniqueId()));

                        main.getplayerData().set(String.valueOf(p.getUniqueId()), Integer.valueOf(pointsAmount + amount));

                        main.saveData();

                        break;
                    }

                }

            }
            else if(args[0].toLowerCase().equals("balance") || args[0].toLowerCase().equals("bal")) {

                if(main.getplayerData().get(String.valueOf(p.getUniqueId())) == null) {

                    String noPointsRaw = main.getSettings().getProperty(Config.MESSAGES_NO_POINTS);
                    String noPoints = ChatColor.translateAlternateColorCodes('&', noPointsRaw);

                    p.sendMessage(noPoints);

                }
                else {

                    String pointsAmount = String.valueOf(main.getplayerData().get(String.valueOf(p.getUniqueId())));

                    String PointsRaw = main.getSettings().getProperty(Config.MESSAGES_POINTS_BALANCE).replace("{points}", pointsAmount);
                    String Points = ChatColor.translateAlternateColorCodes('&', PointsRaw);

                    p.sendMessage(Points);

                }
            }
            else if(args[0].toLowerCase().equals("lb") || args[0].toLowerCase().equals("ld")) {

                List<String> players = new ArrayList<String>();
                List<Integer> amounts = new ArrayList<Integer>();

                ConfigurationSection cf = main.getplayerData();
                cf.getValues(false)
                        .entrySet()
                        .stream()
                        .sorted((a1, a2) -> {
                            int points1 = (int) a1.getValue();
                            int points2 = (int) a2.getValue();
                            return points2 - points1;
                        })
                        .limit(3) // Limit the number of 'results'
                        .forEach(f -> {
                            int points = (int) f.getValue();

                            new BukkitRunnable() {

                                @Override
                                public void run() {
                                    String.valueOf(f);

                                    final OfflinePlayer targetPlayer = Bukkit.getOfflinePlayer(UUID.fromString(f.getKey()));
                                    String name = targetPlayer != null ? targetPlayer.getName() : "Unknown";

                                    players.add(name);
                                    amounts.add(points);

                                    //p.sendMessage(leaderFormat);
                                }

                                ///send name to player
                            }.runTaskAsynchronously(main);

                            //p.sendMessage(String.valueOf(players.indexOf(f.getKey())));
                            //p.sendMessage(f.getKey() + " " + points);
                        });

                //String leaderFormat = ChatColor.translateAlternateColorCodes('&', main.getSettings().getProperty(Config.MESSAGES_LEADERBOARD_PLAYER_FORMAT).replace("{position}", String.valueOf(i)));
                for(int i = 0; i < players.size(); i++) {
                    p.sendMessage(players.get(i) + amounts.get(i));
                }

            }
            else {
                String invalidArgsRaw = main.getSettings().getProperty(Config.MESSAGES_INVALID_COMMAND);
                String invalidArgs = ChatColor.translateAlternateColorCodes('&', invalidArgsRaw);

                p.sendMessage(invalidArgs);
            }

        }
        else {
            ConsoleCommandSender console = getServer().getConsoleSender();
            console.sendMessage("Commands can only be used by players, sorry D:");
        }

        return false;
    }
}
