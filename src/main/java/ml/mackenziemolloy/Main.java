package ml.mackenziemolloy;

import ch.jalu.configme.SettingsManager;
import ch.jalu.configme.SettingsManagerBuilder;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class Main extends JavaPlugin {

    public File playerDataFile;
    public FileConfiguration playerData;

    private final SettingsManager settingsManager = SettingsManagerBuilder
            .withYamlFile(new File(getDataFolder(),"config.yml"))
            .configurationData(Config.class)
            .useDefaultMigrationService()
            .create();

    @Override
    public void onEnable() {

        createPlayerData();

        getServer().getPluginManager().registerEvents(new OnFishCaught(this), this);
        new Commands(this);

        this.getLogger().info(" ");
        this.getLogger().info("Lobby Fishing");
        this.getLogger().info("Made by Mackenzie Molloy");
        this.getLogger().info("(IdConfirmed)");
        this.getLogger().info(" ");

    }

    public SettingsManager getSettings() {
        return settingsManager;
    }

    public FileConfiguration getplayerData() {
        return this.playerData;
    }

    private void createPlayerData() {
        playerDataFile = new File(getDataFolder(), "data.yml");
        if (!playerDataFile.exists()) {
            playerDataFile.getParentFile().mkdirs();
            saveResource("data.yml", false);
        }

        playerData = new YamlConfiguration();
        try {
            playerData.load(playerDataFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void saveData() {
        try {
            playerData.save(playerDataFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadData() {
        try {
            playerData.load(playerDataFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
