package ml.mackenziemolloy;

import ch.jalu.configme.Comment;
import ch.jalu.configme.SettingsHolder;
import ch.jalu.configme.properties.Property;

import java.util.List;

import static ch.jalu.configme.properties.PropertyInitializer.newListProperty;
import static ch.jalu.configme.properties.PropertyInitializer.newProperty;

public class Config implements SettingsHolder {

    /*@Comment("The text of the title")
    public static final Property<String> TITLE_TEXT =
            newProperty("title.text", "-Default-");

    @Comment("The size that the title will have")
    public static final Property<Integer> TITLE_SIZE =
            newProperty("title.size", 10);*/

    @Comment("Fish rarity needed to send message to all players")
    public static final Property<Integer> RARITY_BROADCAST =
            newProperty("rarity_broadcast", 1);

    @Comment("Toggles weather caught items will always be fish")
    public static final Property<Boolean> ALWAYS_FISH =
            newProperty("always_fish", true);

    @Comment("The reload message")
    public static final Property<String> MESSAGES_PLUGIN_RELOADED =
            newProperty("messages.plugin_reloaded", "&bLobbyFishing Reloaded :D");

    @Comment("Message sent to the player when they don't have permission")
    public static final Property<String> MESSAGES_NO_PERMISSION =
            newProperty("messages.no_permission", "&cYou do not have permission!");

    @Comment("Message sent to all when someone catches a fish above specified rarity level")
    public static final Property<String> MESSAGES_FISH_CAUGHT_BROADCAST =
            newProperty("messages.fish_caught_broadcast", "&b[Fisherman Joe] &f{player} has just caught {fish}!");

    @Comment("Message sent to player when they catch a fish below specified rarity level")
    public static final Property<String> MESSAGES_FISH_CAUGHT_MSG =
            newProperty("messages.fish_caught_msg", "&b[Fisherman Joe] &fYou have just caught {fish}!");

    @Comment("Message sent to player when check their balance without having any money")
    public static final Property<String> MESSAGES_NO_POINTS =
            newProperty("messages.no_points", "&b[Fisherman Joe] &fYou do not have any points yet! Maybe catch and sell some fishies?");

    @Comment("Message sent to player when check their balance without having any money")
    public static final Property<String> MESSAGES_POINTS_BALANCE =
            newProperty("messages.points_balance", "&b[Fisherman Joe] &fYou have {points} points.");

    @Comment("Message sent to player when check their balance without having any money")
    public static final Property<String> MESSAGES_POINTS_GAINED =
            newProperty("messages.points_gained", "&b[Fisherman Joe] &fYou earned {gained} points. &7({quantity} Fish)");

    @Comment("Message sent to player when they request the help menu")
    public static final Property<List<String>> MESSAGES_HELP =
            newListProperty("messages.help", "&3&lLobby Fishing &7by Mackenzie Molloy", "&7", "&b/lf &f returns this help message", "&b/lf sell &f sells held item", "&b/lf bal &f returns your balance", "&b/lf reload &freloads plugin configuration", "&f");

    @Comment("Message sent to player when they send an invalid command")
    public static final Property<String> MESSAGES_INVALID_COMMAND =
            newProperty("messages.invalid_command", "&b[Fishman Joe] &fUnknown sub-command, have a look at &b/lf&f for help.");

    @Comment("Message sent to player when attempting to sell an unsellable item")
    public static final Property<String> MESSAGES_NOT_SELLABLE =
            newProperty("messages.not_sellable", "&b[Fisherman Joe] &fThe item you are holding isn't sellable.");

    @Comment("List Of Fish Rarities")
    public static final Property<List<String>> MESSAGES_FISH_RARITIES =
            newListProperty("messages.fish_rarities", "&aCommon", "&8Uncommon", "&bRare", "&5Epic", "&6Legendary");

    @Comment("List Of Fishes")
    public static final Property<List<String>> MESSAGES_FISHES =
            newListProperty("messages.fishes", "Carp", "Salmon", "Cod", "Blue Tang", "Bluegill", "Ayu", "Catfish", "trout", "clownfish");

    @Comment("Fish message format")
    public static final Property<String> MESSAGES_FISH_FORMAT =
            newProperty("messages.fish_format", "{rarity} {fish}");

    @Comment("Leaderboard Player Format")
    public static final Property<String> MESSAGES_LEADERBOARD_PLAYER_FORMAT =
            newProperty("messages.LEADERBOARD_PLAYER_FORMAT", "&3{position} &f{player}: &b{points}");

    @Comment("Junk Item name")
    public static final Property<String> MESSAGES_JUNK_NAME =
            newProperty("messages.junk_name", "&8Junk");


    private Config() {
    }

}
