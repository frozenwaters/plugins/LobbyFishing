package ml.mackenziemolloy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class OnFishCaught implements Listener {

    private final Main main;

    public OnFishCaught(final Main main) {
        this.main = main;
    }

    @EventHandler
    public void onFish(PlayerFishEvent event) {

        if (event.getState().equals(PlayerFishEvent.State.CAUGHT_FISH)) { //event.getState().equals(PlayerFishEvent.State.CAUGHT_FISH)

            Item fishItem = (Item) event.getCaught();
            ItemStack fishStack = fishItem.getItemStack();

            if(main.getSettings().getProperty(Config.ALWAYS_FISH) == true) {
                if(String.valueOf(fishStack.getType()) != "LEGACY_RAW_FISH") {
                    fishStack.setType(Material.valueOf("LEGACY_RAW_FISH"));
                }
            }

            if (String.valueOf(fishStack.getType()) == "LEGACY_RAW_FISH") {
                List<String> raritiesColor = main.getSettings().getProperty(Config.MESSAGES_FISH_RARITIES);
                int rarity = (int) (Math.random() * raritiesColor.size());

                List<String> fishesColor = main.getSettings().getProperty(Config.MESSAGES_FISHES);
                int FishPick = (int) (Math.random() * fishesColor.size());
                String fish = fishesColor.get(FishPick);

                String fishFormat = ChatColor.translateAlternateColorCodes('&', main.getSettings().getProperty(Config.MESSAGES_FISH_FORMAT).replace("{rarity}", raritiesColor.get(rarity)).replace("{fish}", fish));
                int RarityNeeded = main.getSettings().getProperty(Config.RARITY_BROADCAST);

                ItemMeta fishMeta = fishStack.getItemMeta();
                fishMeta.setDisplayName(fishFormat);

                fishStack.setItemMeta(fishMeta);

                if (rarity < RarityNeeded) {
                    String YouCaughtRaw = main.getSettings().getProperty(Config.MESSAGES_FISH_CAUGHT_MSG).replace("{fish}", fishFormat);
                    String YouCaught = ChatColor.translateAlternateColorCodes('&', YouCaughtRaw);

                    event.getPlayer().sendMessage(YouCaught);
                    return;
                } else if (rarity >= RarityNeeded) {
                    String BCCaughtRaw = main.getSettings().getProperty(Config.MESSAGES_FISH_CAUGHT_BROADCAST).replace("{fish}", fishFormat).replace("{player}", event.getPlayer().getDisplayName());
                    String BCCaught = ChatColor.translateAlternateColorCodes('&', BCCaughtRaw);

                    Bukkit.broadcastMessage(BCCaught);
                    return;
                }
            }

            else {
                ItemMeta fishMeta = fishStack.getItemMeta();
                fishMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.getSettings().getProperty(Config.MESSAGES_JUNK_NAME)));

                fishStack.setItemMeta(fishMeta);

                String YouCaughtRaw = main.getSettings().getProperty(Config.MESSAGES_FISH_CAUGHT_MSG).replace("{fish}", main.getSettings().getProperty(Config.MESSAGES_JUNK_NAME));
                String YouCaught = ChatColor.translateAlternateColorCodes('&', YouCaughtRaw);

                event.getPlayer().sendMessage(YouCaught);
            }
        }
    }

}
